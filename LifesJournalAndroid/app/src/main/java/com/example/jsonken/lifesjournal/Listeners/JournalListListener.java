package com.example.jsonken.lifesjournal.Listeners;

public interface JournalListListener {
    void getSelectedEntry(int _position);
}
