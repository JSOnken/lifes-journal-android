package com.example.jsonken.lifesjournal.Fragments;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.jsonken.lifesjournal.Adapters.CustomAdapter;
import com.example.jsonken.lifesjournal.DetailActivity;
import com.example.jsonken.lifesjournal.EntryActivity;
import com.example.jsonken.lifesjournal.Listeners.TakePhotoListener;
import com.example.jsonken.lifesjournal.Objects.JournalEntry;
import com.example.jsonken.lifesjournal.Objects.JournalHelper;
import com.example.jsonken.lifesjournal.R;

public class JournalListFragment extends ListFragment {

    public static final String TAG = "JournalListFragment.TAG";
    private TakePhotoListener mListener;
    JournalHelper mHelper;

    public static JournalListFragment newInstance() {

        Bundle args = new Bundle();

        JournalListFragment fragment = new JournalListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.journal_list_fragment, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof TakePhotoListener) {
            mListener = (TakePhotoListener) context;
        } else {
            throw new IllegalArgumentException("Containing context does not implement the " +
                    "TakePhotoListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mHelper = JournalHelper.getInstance();

        CustomAdapter adapter = new CustomAdapter(getActivity(), mHelper.getJournalEntries());

        setListAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.journal_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent(getActivity(), EntryActivity.class);

        switch (item.getItemId()) {
            case R.id.menu_text:
                intent.putExtra(EntryActivity.ADD_ENTRY_REQUEST, "text");
                startActivity(intent);
                break;
            case R.id.menu_camera:
                mListener.takePhoto();
                break;
            case R.id.menu_audio:
                intent.putExtra(EntryActivity.ADD_ENTRY_REQUEST, "audio");
                startActivity(intent);
                break;
        }

        return true;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        JournalEntry selectedEntry = mHelper.getSelectedEntry(position);

        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(DetailActivity.SELECTED_ITEM_POSITION, position);

        if (selectedEntry.getmPhotoURL() == null) {
            intent.putExtra(DetailActivity.OPEN_DETAILS_REQUEST, "text");
            startActivity(intent);
        } else if (selectedEntry.getmPhotoURL() != null) {
            intent.putExtra(DetailActivity.OPEN_DETAILS_REQUEST, "photo");
            startActivity(intent);
        }
    }
}
