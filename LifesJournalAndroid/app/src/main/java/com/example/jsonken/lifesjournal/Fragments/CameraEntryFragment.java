package com.example.jsonken.lifesjournal.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.jsonken.lifesjournal.Listeners.EntryListener;
import com.example.jsonken.lifesjournal.Objects.JournalEntry;
import com.example.jsonken.lifesjournal.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CameraEntryFragment extends Fragment {

    public static final String TAG = "CameraEntryFragment.TAG";
    private static String mPhotoPath;
    private EntryListener mListener;

    public static CameraEntryFragment newInstance(String _photoPath) {

        Bundle args = new Bundle();

        mPhotoPath = _photoPath;
        CameraEntryFragment fragment = new CameraEntryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle
            savedInstanceState) {

        View view = inflater.inflate(R.layout.photo_entry_fragment, container, false);

        ImageView photo = view.findViewById(R.id.imageView_photoEntry);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;

        Bitmap bitmap = BitmapFactory.decodeFile(mPhotoPath, options);
        photo.setImageBitmap(bitmap);

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof EntryListener) {
            mListener = (EntryListener) context;
        } else {
            throw new IllegalArgumentException("Containing context does not implement the " +
                    "EntryListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        EditText entryET = getView().findViewById(R.id.editText_photoEntry);
        String entryString = entryET.getText().toString();

        if (entryString.trim().length() != 0) {

            JournalEntry entry =
                    new JournalEntry(getDateTime(), entryString, mPhotoPath, null, null, null);
            mListener.addEntry(entry);
        } else {
            Toast.makeText(getActivity(), "You must add an entry before saving", Toast.LENGTH_SHORT)
                    .show();
        }

        return true;
    }

    private String getDateTime() {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

        return dateFormat.format(date);
    }
}
