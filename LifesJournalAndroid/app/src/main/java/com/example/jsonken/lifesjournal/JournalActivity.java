package com.example.jsonken.lifesjournal;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.jsonken.lifesjournal.Fragments.CalendarFragment;
import com.example.jsonken.lifesjournal.Fragments.JournalListFragment;
import com.example.jsonken.lifesjournal.Fragments.MapFragment;
import com.example.jsonken.lifesjournal.Listeners.EditEntryListener;
import com.example.jsonken.lifesjournal.Listeners.TakePhotoListener;
import com.example.jsonken.lifesjournal.Objects.JournalEntry;
import com.example.jsonken.lifesjournal.Objects.JournalHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class JournalActivity extends AppCompatActivity implements TakePhotoListener {

    private static final int REQUEST_TAKE_PICTURE = 0x01010;
    public static final String TAG = "JournalActivity.TAG";

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private FirebaseUser mUser;
    private JournalHelper mHelper;
    private BottomNavigationView navigation;
    private String mTimeStamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        mUser = mAuth.getCurrentUser();

        mHelper = JournalHelper.getInstance();

        downloadEntries();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_journal:
                    getFragmentManager().beginTransaction()
                            .replace(R.id.navigation_fragment_container,
                                    JournalListFragment.newInstance(), JournalListFragment.TAG)
                            .commit();
                    return true;
                case R.id.navigation_calendar:
                    getFragmentManager().beginTransaction()
                            .replace(R.id.navigation_fragment_container,
                                    CalendarFragment.newInstance(), CalendarFragment.TAG)
                            .commit();
                    return true;
                case R.id.navigation_map:
                    getFragmentManager().beginTransaction()
                            .replace(R.id.navigation_fragment_container,
                                    MapFragment.newInstance(), MapFragment.TAG)
                            .commit();
                    return true;
            }
            return false;
        }
    };

    private void downloadEntries() {
        mHelper.setJournalArrayList(new ArrayList<JournalEntry>());
        db.collection("users").document(mUser.getEmail()).collection("journal entries")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document: task.getResult()) {
                                String docID = document.getId();
                                if (document.get("entry") != null) {
                                    String textEntry = document.get("entry").toString();
                                    if (document.get("downloadURL") != null) {
                                        String photoURL = document.get("downloadURL").toString();
                                        JournalEntry entry = new JournalEntry(docID, textEntry,
                                                photoURL, null, null, null);
                                        mHelper.addJournalEntry(entry);
                                    } else {
                                        JournalEntry journalEntry = new JournalEntry(docID, textEntry,

                                                null, null, null, null);
                                        mHelper.addJournalEntry(journalEntry);
                                    }
                                }
                            }
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                        navigation = findViewById(R.id.navigation);
                        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
                        navigation.setSelectedItemId(R.id.navigation_journal);
                    }
                });
    }

    @Override
    public void takePhoto() {
        mTimeStamp = getTimestampString();
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, getOutputUri());
        startActivityForResult(cameraIntent, REQUEST_TAKE_PICTURE);
    }

    private String getTimestampString() {
        Long tempTS = System.currentTimeMillis()/1000;

        return tempTS.toString();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String photoPath = getOutputFilePath();
        Intent intent = new Intent(this, EntryActivity.class);
        intent.putExtra(EntryActivity.ADD_ENTRY_REQUEST, "camera");
        intent.putExtra(EntryActivity.TAKE_PHOTO_REQUEST, photoPath);
        startActivity(intent);
    }

    private Uri getOutputUri() {
        File storageDir = this.getExternalFilesDir("images");
        File imageFile = new File(storageDir, "lifesjournal" + mTimeStamp + ".jpg");
        try {
            imageFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return FileProvider.getUriForFile(this, getString(R.string.fileProvider), imageFile);
    }

    private String getOutputFilePath() {
        File protectedStorage = this.getExternalFilesDir("images");
        File imageFile = new File(protectedStorage, "lifesjournal" + mTimeStamp + ".jpg");
        try {
            imageFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageFile.getAbsolutePath();
    }
}
