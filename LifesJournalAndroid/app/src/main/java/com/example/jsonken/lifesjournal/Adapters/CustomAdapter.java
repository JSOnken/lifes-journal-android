package com.example.jsonken.lifesjournal.Adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jsonken.lifesjournal.Objects.JournalEntry;
import com.example.jsonken.lifesjournal.R;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<JournalEntry> {

    private Context mContext;
    private ArrayList<JournalEntry> mEntries;

    public CustomAdapter(@NonNull Context _context, ArrayList<JournalEntry> _entriesArray) {
        super(_context, 0, _entriesArray);
        this.mContext = _context;
        mEntries = _entriesArray;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItem = convertView;

        if (listItem == null) {
            listItem = LayoutInflater.from(mContext)
                    .inflate(R.layout.adapter_layout, parent, false);
        }

        JournalEntry entry = mEntries.get(position);

        ImageView imageView = listItem.findViewById(R.id.imageView_adapter);
        if (entry.getmPhotoURL() != null) {
            Picasso.get()
                    .load(entry.getmPhotoURL())
                    .resize(200, 200)
                    .centerCrop()
                    .into(imageView);
        }

        TextView textEntry = listItem.findViewById(R.id.textView_entry_adapter);
        textEntry.setText(entry.getmTextEntry());

        TextView dateEntry = listItem.findViewById(R.id.textView_date_adapter);
        dateEntry.setText(entry.getmDateEntered());

        return listItem;
    }
}
