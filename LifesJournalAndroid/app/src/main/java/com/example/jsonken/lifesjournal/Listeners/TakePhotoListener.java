package com.example.jsonken.lifesjournal.Listeners;

public interface TakePhotoListener {
    void takePhoto();
}
