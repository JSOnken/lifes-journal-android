package com.example.jsonken.lifesjournal.Listeners;

import com.example.jsonken.lifesjournal.Objects.JournalEntry;

public interface EditEntryListener {
    void editTextEntry(JournalEntry _entry);
    void editPhotoEntry(JournalEntry _entry);
    void deleteEntry(JournalEntry _entry);
}
