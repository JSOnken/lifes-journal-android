package com.example.jsonken.lifesjournal.Objects;

public class JournalEntry {

    private final String mDateEntered;
    private String mTextEntry;
    private final String mPhotoURL;
    private final String mAudioURL;
    private final Double mLatitude;
    private final Double mLongitude;

    public JournalEntry(String _dateEntered, String _textEntry, String _photoURL, String
            _audioURL, Double _latitude, Double _longitude) {
        this.mDateEntered = _dateEntered;
        this.mTextEntry = _textEntry;
        this.mPhotoURL = _photoURL;
        this.mAudioURL = _audioURL;
        this.mLatitude = _latitude;
        this.mLongitude = _longitude;
    }

    public String getmDateEntered() {
        return mDateEntered;
    }

    public String getmTextEntry() {
        return mTextEntry;
    }

    public String getmPhotoURL() {
        return mPhotoURL;
    }

    public void setTextEntry(String _textEntry) {
        this.mTextEntry = _textEntry;
    }

    @Override
    public String toString() {
        return mTextEntry + "\n" + mDateEntered;
    }
}
