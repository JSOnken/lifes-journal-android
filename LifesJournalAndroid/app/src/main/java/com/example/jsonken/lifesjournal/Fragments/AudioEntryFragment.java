package com.example.jsonken.lifesjournal.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jsonken.lifesjournal.R;

public class AudioEntryFragment extends Fragment {

    public static final String TAG = "AudioEntryFragment.TAG";

    public static AudioEntryFragment newInstance() {

        Bundle args = new Bundle();

        AudioEntryFragment fragment = new AudioEntryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle
            savedInstanceState) {

        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.future_update_fragment, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
}
