package com.example.jsonken.lifesjournal;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.jsonken.lifesjournal.Fragments.PhotoDetailFragment;
import com.example.jsonken.lifesjournal.Fragments.TextDetailFragment;
import com.example.jsonken.lifesjournal.Listeners.EditEntryListener;
import com.example.jsonken.lifesjournal.Objects.JournalEntry;
import com.example.jsonken.lifesjournal.Objects.JournalHelper;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class DetailActivity extends AppCompatActivity implements EditEntryListener {

    public static final String TAG = "DetailActivity.TAG";
    public static final String OPEN_DETAILS_REQUEST =
            "com.example.jsonken.lifesjournal.OPEN_DETAILS_REQUEST";
    public static final String SELECTED_ITEM_POSITION =
            "com.example.jsonken.lifesjournal.SELECTED_ITEM_POSITION";

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private FirebaseUser mUser;
    private FirebaseStorage mStorage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frame_layout);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance();

        mUser = mAuth.getCurrentUser();

        Intent startIntent = getIntent();
        String sendingAction = startIntent.getStringExtra(OPEN_DETAILS_REQUEST);
        Integer position = startIntent.getIntExtra(SELECTED_ITEM_POSITION, -1);
        JournalHelper helper = JournalHelper.getInstance();
        JournalEntry entry = helper.getSelectedEntry(position);

        if (savedInstanceState == null) {
            switch (sendingAction) {
                case "text":
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container,
                                    TextDetailFragment.newInstance(entry),
                                    TextDetailFragment.TAG)
                            .commit();
                    break;
                case "photo":
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container,
                                    PhotoDetailFragment.newInstance(entry),
                                    PhotoDetailFragment.TAG)
                            .commit();
                    break;
            }
        }
    }

    @Override
    public void editTextEntry(JournalEntry _entry) {
        Map<String, String> entry = new HashMap<>();
        entry.put("entry", _entry.getmTextEntry());

        db.collection("users").document(mUser.getEmail()).collection("journal entries")
                .document(_entry.getmDateEntered())
                .set(entry)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Upload text document successful");
                        Intent intent = new Intent(getApplicationContext(), JournalActivity.class);

                        startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }

    @Override
    public void editPhotoEntry(final JournalEntry _entry) {
        Map<String, String> entry = new HashMap<>();
        entry.put("entry", _entry.getmTextEntry());
        entry.put("downloadURL", _entry.getmPhotoURL());

        db.collection("users").document(mUser.getEmail()).collection("journal entries")
                .document(_entry.getmDateEntered())
                .set(entry)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Upload photo document successful");
                        Intent intent = new Intent(getApplicationContext(), JournalActivity.class);

                        startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }

    @Override
    public void deleteEntry(JournalEntry _entry) {
        db.collection("users").document(mUser.getEmail()).collection("journal entries")
                .document(_entry.getmDateEntered())
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting document", e);
                    }
                });
        Intent intent = new Intent(this, JournalActivity.class);
        startActivity(intent);
    }
}
