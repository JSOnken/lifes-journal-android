package com.example.jsonken.lifesjournal.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.jsonken.lifesjournal.Listeners.EditEntryListener;
import com.example.jsonken.lifesjournal.Listeners.EntryListener;
import com.example.jsonken.lifesjournal.Objects.JournalEntry;
import com.example.jsonken.lifesjournal.R;
import com.squareup.picasso.Picasso;

public class PhotoDetailFragment extends Fragment {

    public static final String TAG = "PhotoDetailFragment.TAG";
    private static JournalEntry mEntry;
    private EditEntryListener mListener;
    private EditText mDetails;

    public static PhotoDetailFragment newInstance(JournalEntry _entry) {

        Bundle args = new Bundle();

        mEntry = _entry;
        PhotoDetailFragment fragment = new PhotoDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle
            savedInstanceState) {

        View view = inflater.inflate(R.layout.photo_entry_fragment, container, false);

        ImageView imageView = view.findViewById(R.id.imageView_photoEntry);
        Picasso.get().load(mEntry.getmPhotoURL()).into(imageView);

        mDetails = view.findViewById(R.id.editText_photoEntry);
        mDetails.setText(mEntry.getmTextEntry());

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof EditEntryListener) {
            mListener = (EditEntryListener) context;
        } else {
            throw new IllegalArgumentException("Containing context does not implement the " +
                    "EditEntryListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mEntry.setTextEntry(mDetails.getText().toString());
        switch (item.getItemId()) {
            case R.id.action_delete:
                mListener.deleteEntry(mEntry);
                break;
            case R.id.action_save_detail:
                mListener.editPhotoEntry(mEntry);
                break;
        }

        return true;
    }
}
