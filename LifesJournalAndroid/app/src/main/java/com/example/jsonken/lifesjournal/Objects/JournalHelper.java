package com.example.jsonken.lifesjournal.Objects;

import java.util.ArrayList;

public class JournalHelper {

    private static JournalHelper mInstance;
    private static ArrayList<JournalEntry> mJournalArrayList = new ArrayList<>();
    private JournalEntry mEntry;

    private JournalHelper() {}

    public static JournalHelper getInstance() {
        if (mInstance == null) {
            mInstance = new JournalHelper();
        }

        return mInstance;
    }

    public void addJournalEntry(JournalEntry _entry) {
        mJournalArrayList.add(_entry);
    }

    public ArrayList<JournalEntry> getJournalEntries() {
        return mJournalArrayList;
    }

    public void setJournalArrayList(ArrayList<JournalEntry> _journalEntries) {
        mJournalArrayList = _journalEntries;
    }

    public JournalEntry getSelectedEntry(int _position) {
        return mJournalArrayList.get(_position);
    }

    public void setEntry(JournalEntry _entry) {
        this.mEntry = _entry;
    }

    public JournalEntry getEntry() {
        return mEntry;
    }
}
