package com.example.jsonken.lifesjournal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.jsonken.lifesjournal.Fragments.SignInOptionsFragment;
import com.example.jsonken.lifesjournal.Objects.JournalEntry;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignInActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frame_layout);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser mUser = mAuth.getCurrentUser();

        if (mUser != null) {
            Intent intent = new Intent(this, JournalActivity.class);
            startActivity(intent);
            Log.d("SignInActivity", "User was found!");
        } else {
            getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    SignInOptionsFragment.newInstance(), SignInOptionsFragment.TAG)
                    .commit();
            Log.d("SignInActivity", "User was not found!");
        }
    }
}
