package com.example.jsonken.lifesjournal;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.jsonken.lifesjournal.Fragments.AudioEntryFragment;
import com.example.jsonken.lifesjournal.Fragments.CameraEntryFragment;
import com.example.jsonken.lifesjournal.Fragments.TextEntryFragment;
import com.example.jsonken.lifesjournal.Listeners.EntryListener;
import com.example.jsonken.lifesjournal.Objects.JournalEntry;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class EntryActivity extends AppCompatActivity implements EntryListener {

    public static final String TAG = "EntryActivity.TAG";
    public static final String ADD_ENTRY_REQUEST =
            "com.example.jsonken.lifesjournal.ADD_ENTRY_REQUEST";
    public static final String TAKE_PHOTO_REQUEST =
            "com.example.jsonken.lifesjournal.TAKE_PHOTO_REQUEST";

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private FirebaseUser mUser;
    private FirebaseStorage mStorage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frame_layout);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance();

        mUser = mAuth.getCurrentUser();

        Intent startIntent = getIntent();
        String sendingAction = startIntent.getStringExtra(ADD_ENTRY_REQUEST);
        String photoPath = startIntent.getStringExtra(TAKE_PHOTO_REQUEST);

        if (savedInstanceState == null) {
            switch (sendingAction) {
                case "text":
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container,
                                    TextEntryFragment.newInstance(), TextEntryFragment.TAG)
                            .commit();
                    break;
                case "camera":
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container,
                                    CameraEntryFragment.newInstance(photoPath), CameraEntryFragment.TAG)
                            .commit();
                    break;
                case "audio":
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container,
                                    AudioEntryFragment.newInstance(), AudioEntryFragment.TAG)
                            .commit();
                    break;
            }
        }
    }

    @Override
    public void addEntry(JournalEntry _entry) {

        if (_entry.getmPhotoURL() == null) {
            Map<String, String> entry = new HashMap<>();
            entry.put("entry", _entry.getmTextEntry());

            db.collection("users").document(mUser.getEmail()).collection("journal entries")
                    .document(_entry.getmDateEntered())
                    .set(entry)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(TAG, "Upload document successful");
                            Intent intent = new Intent(getApplicationContext(), JournalActivity.class);

                            startActivity(intent);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error adding document", e);
                        }
                    });
        } else if (_entry.getmPhotoURL() != null) {
            uploadPhoto(_entry);
        }
    }

    public void uploadPhoto(final JournalEntry _entry) {
        Uri file = Uri.fromFile(new File(_entry.getmPhotoURL()));

        final StorageReference storageRef =
                mStorage.getReference("/Photos/" + mUser.getEmail() + "/" +
                        _entry.getmDateEntered() + ".jpg");
        UploadTask uploadTask = storageRef.putFile(file);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                return storageRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    String photoURL = downloadUri.toString();

                    Map<String, String> entry = new HashMap<>();
                    entry.put("entry", _entry.getmTextEntry());
                    entry.put("downloadURL", photoURL);

                    db.collection("users").document(mUser.getEmail()).collection("journal entries")
                            .document(_entry.getmDateEntered())
                            .set(entry)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d(TAG, "Upload document successful");
                                    Intent intent = new Intent(getApplicationContext(), JournalActivity.class);

                                    startActivity(intent);
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error adding document", e);
                                }
                            });

                } else {
                    Log.e(TAG, "Error uploading photo entry task");
                }
            }
        });
    }
}
