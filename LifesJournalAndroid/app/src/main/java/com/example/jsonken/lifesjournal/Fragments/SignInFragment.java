package com.example.jsonken.lifesjournal.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jsonken.lifesjournal.JournalActivity;
import com.example.jsonken.lifesjournal.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignInFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "SignInFragment.TAG";
    private FirebaseAuth mAuth;

    private EditText emailET;
    private EditText passwordET;

    public static SignInFragment newInstance() {

        Bundle args = new Bundle();

        SignInFragment fragment = new SignInFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle
            savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();

        return inflater.inflate(R.layout.sign_in_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        emailET = getActivity().findViewById(R.id.editText_email_signIn);
        passwordET = getActivity().findViewById(R.id.editText_password_signIn);
        getActivity().findViewById(R.id.button_signUp).setOnClickListener(this);
        getActivity().findViewById(R.id.button_signIn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_signUp:
                getActivity().getFragmentManager().beginTransaction().replace(R.id
                                .fragment_container,
                        CreateAccountFragment.newInstance(), CreateAccountFragment.TAG)
                        .commit();
                break;
            case R.id.button_signIn:
                String email = emailET.getText().toString();
                String password = passwordET.getText().toString();

                if (email.trim().length() != 0 && password.trim().length() != 0) {
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(getActivity(), new
                                    OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                Log.d(TAG, "createUserWithEmail: Success");
                                                FirebaseUser user = mAuth.getCurrentUser();
                                                Log.d(TAG, "onComplete: " + user);
                                                Intent intent = new Intent(getActivity(),
                                                        JournalActivity.class);
                                                startActivity(intent);
                                            } else {
                                                Log.w(TAG, "createUserWithEmail: failure", task.getException());

                                                Toast.makeText(getActivity(), "Authentication " +
                                                                "failed.",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                } else {
                    Toast.makeText(getActivity(), "All fields must have values",
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
