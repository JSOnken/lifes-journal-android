package com.example.jsonken.lifesjournal.Listeners;

import com.example.jsonken.lifesjournal.Objects.JournalEntry;

public interface EntryListener {
    void addEntry(JournalEntry _entry);
}
