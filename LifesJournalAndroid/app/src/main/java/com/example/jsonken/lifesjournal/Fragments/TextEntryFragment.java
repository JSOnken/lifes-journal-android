package com.example.jsonken.lifesjournal.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jsonken.lifesjournal.Listeners.EntryListener;
import com.example.jsonken.lifesjournal.Objects.JournalEntry;
import com.example.jsonken.lifesjournal.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TextEntryFragment extends Fragment {

    public static final String TAG = "TextEntryFragment.TAG";
    private EntryListener mListener;

    public static TextEntryFragment newInstance() {

        Bundle args = new Bundle();

        TextEntryFragment fragment = new TextEntryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle
            savedInstanceState) {

        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.text_entry_fragment, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof EntryListener) {
            mListener = (EntryListener) context;
        } else {
            throw new IllegalArgumentException("Containing context does not implement the " +
                    "EntryListener");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        EditText entryET = getView().findViewById(R.id.editText_textEntry);
        String entryString = entryET.getText().toString();

        if (entryString.trim().length() != 0) {

            JournalEntry entry =
                    new JournalEntry(getDateTime(), entryString, null, null, null, null);
            mListener.addEntry(entry);
        } else {
            Toast.makeText(getActivity(), "You must add an entry before saving", Toast.LENGTH_SHORT)
                    .show();
        }

        return true;
    }

    private String getDateTime() {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

        return dateFormat.format(date);
    }
}
