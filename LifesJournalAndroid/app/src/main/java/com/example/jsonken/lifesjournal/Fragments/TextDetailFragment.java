package com.example.jsonken.lifesjournal.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jsonken.lifesjournal.Listeners.EditEntryListener;
import com.example.jsonken.lifesjournal.Objects.JournalEntry;
import com.example.jsonken.lifesjournal.R;

public class TextDetailFragment extends Fragment {

    public static final String TAG = "TextDetailFragment.TAG";
    private static JournalEntry mEntry;
    private EditEntryListener mListener;
    private EditText mDetails;

    public static TextDetailFragment newInstance(JournalEntry _entry) {

        Bundle args = new Bundle();

        mEntry = _entry;
        TextDetailFragment fragment = new TextDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle
            savedInstanceState) {

        View view = inflater.inflate(R.layout.text_entry_fragment, container, false);

        mDetails = view.findViewById(R.id.editText_textEntry);
        mDetails.setText(mEntry.getmTextEntry());

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof EditEntryListener) {
            mListener = (EditEntryListener) context;
        } else {
            throw new IllegalArgumentException("Containing context does not implement the " +
                    "EditEntryListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mEntry.setTextEntry(mDetails.getText().toString());
        switch (item.getItemId()) {
            case R.id.action_delete:
                mListener.deleteEntry(mEntry);
                break;
            case R.id.action_save_detail:
                mListener.editTextEntry(mEntry);
                break;
        }
        return true;

    }
}
