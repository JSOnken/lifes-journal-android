package com.example.jsonken.lifesjournal.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jsonken.lifesjournal.JournalActivity;
import com.example.jsonken.lifesjournal.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class CreateAccountFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "CreateAccountFragment.TAG";
    private FirebaseAuth mAuth;
    private EditText mEmailET;
    private EditText mPasswordET;
    private EditText mConfirmPasswordET;


    public static CreateAccountFragment newInstance() {

        Bundle args = new Bundle();

        CreateAccountFragment fragment = new CreateAccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle
            savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();

        return inflater.inflate(R.layout.create_account_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mEmailET = getActivity().findViewById(R.id.editText_email_create);
        mPasswordET = getActivity().findViewById(R.id.editText_password_create);
        mConfirmPasswordET = getActivity().findViewById(R.id.editText_confirmPassword_create);
        getActivity().findViewById(R.id.button_create).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String email = mEmailET.getText().toString();
        String password = mPasswordET.getText().toString();
        String confirmPassword = mConfirmPasswordET.getText().toString();

        if (email.trim().length() != 0 && password.trim().length() != 0 &&
                confirmPassword.trim().length() != 0) {
           mAuth.createUserWithEmailAndPassword(email, password)
                   .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                       @Override
                       public void onComplete(@NonNull Task<AuthResult> task) {
                           if (task.isSuccessful()) {
                               Log.d("CreateAccount", "createUserWithEmail:success");
                               FirebaseUser user = mAuth.getCurrentUser();
                               Log.d("CreateAccount", "onComplete: " + user);
                               Intent intent = new Intent(getActivity(),
                                       JournalActivity.class);
                               startActivity(intent);
                           } else {
                               Log.w("CreateAccount", "createUserWithEmail:failure", task.getException());
                               Toast.makeText(getActivity(), "Authentication failed.",
                                       Toast.LENGTH_SHORT).show();
                           }
                       }
                   });
        } else {
            Toast.makeText(getActivity(), "All fields must have values", Toast.LENGTH_SHORT).show();
        }
    }
}
